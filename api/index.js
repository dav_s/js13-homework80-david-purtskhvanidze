const express = require('express');
const cors = require('cors');
const db = require('./mySqlDb');
const categories = require('./app/categories');
const locations = require('./app/locations');
const subjects = require('./app/subjects');
const app = express();

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/categories', categories);
app.use('/locations', locations);
app.use('/subjects', subjects);

const run = async () => {
    await db.init();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
};

run().catch(e => console.error(e));