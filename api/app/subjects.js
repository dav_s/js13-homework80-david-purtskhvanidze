const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT id, category_id, location_id, title FROM subjects';

        if (req.query.filter === 'image') {
            query += ' WHERE image IS NOT NULL';
        }

        let [subjects] = await db.getConnection().execute(query);

        return res.send(subjects);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const [subjects] = await db.getConnection().execute('SELECT * FROM subjects WHERE id = ?', [req.params.id]);

        const subject = subjects[0];

        if (!subject) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(subject);
    } catch (e) {
        next(e);
    }
});

router.get('/delete/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM subjects WHERE id = ?', [req.params.id]);

        return res.send({message: 'Subject with id=' + req.params.id +' is deleted'});
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.category_id || !req.body.location_id || !req.body.title) {
            return res.status(400).send({message: 'Category ID, location ID and title are required'});
        }

        const subject = {
            category_id: req.body.category_id,
            location_id: req.body.location_id,
            title: req.body.title,
            description: req.body.description,
            image: null,
        };

        if (req.file) {
            subject.image = req.file.filename;
        }

        let query = 'INSERT INTO subjects (category_id, location_id, title, description, image) VALUES (?, ?, ?, ?, ?)';

        const [results] = await db.getConnection().execute(query, [
            subject.category_id,
            subject.location_id,
            subject.title,
            subject.description,
            subject.image
        ]);

        subject.id = results.insertId;

        return res.send(subject);
    } catch (e) {
        next(e);
    }
});

module.exports = router;