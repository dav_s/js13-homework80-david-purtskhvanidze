const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT id, title FROM locations';

        let [locations] = await db.getConnection().execute(query);

        return res.send(locations);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const [locations] = await db.getConnection().execute('SELECT * FROM locations WHERE id = ?', [req.params.id]);

        const location = locations[0];

        if (!location) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(location);
    } catch (e) {
        next(e);
    }
});

router.get('/delete/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM locations WHERE id = ?', [req.params.id]);

        return res.send({message: 'Location with id=' + req.params.id +' is deleted'});
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.title) {
            return res.status(400).send({message: 'Title is required'});
        }

        const location = {
            title: req.body.title,
            description: req.body.description,
        };

        let query = 'INSERT INTO locations (title, description) VALUES (?, ?)';

        const [results] = await db.getConnection().execute(query, [
            location.title,
            location.description,
        ]);

        location.id = results.insertId;

        return res.send(location);
    } catch (e) {
        next(e);
    }
});

module.exports = router;