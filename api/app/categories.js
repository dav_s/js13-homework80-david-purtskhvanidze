const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT id, title FROM categories';

        let [categories] = await db.getConnection().execute(query);

        return res.send(categories);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const [categories] = await db.getConnection().execute('SELECT * FROM categories WHERE id = ?', [req.params.id]);

        const category = categories[0];

        if (!category) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(category);
    } catch (e) {
        next(e);
    }
});

router.get('/delete/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM categories WHERE id = ?', [req.params.id]);

        return res.send({message: 'Category with id=' + req.params.id +' is deleted'});
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.title) {
            return res.status(400).send({message: 'Title is required'});
        }

        const category = {
            title: req.body.title,
            description: req.body.description,
        };

        let query = 'INSERT INTO categories (title, description) VALUES (?, ?)';

        const [results] = await db.getConnection().execute(query, [
            category.title,
            category.description,
        ]);

        category.id = results.insertId;

        return res.send(category);
    } catch (e) {
        next(e);
    }
});

module.exports = router;