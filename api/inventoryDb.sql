create schema inventory_david_js13_database collate utf8_general_ci;
use inventory_david_js13_database;

create table categories
(
    id          int auto_increment primary key,
    title       varchar(255)       not null,
    description text               null
);

create table locations
(
    id          int auto_increment primary key,
    title       varchar(255)       not null,
    description text               null
);

create table subjects
(
    id          int auto_increment primary key,
    category_id int                null,
    location_id int                null,
    title       varchar(255)       not null,
    description text               null,
    image       varchar(32)        null,

    constraint subjects_categories_id_fk
        foreign key (category_id) references categories (id)
            on update cascade on delete set null,

    constraint subjects_locations_id_fk
        foreign key (location_id) references locations (id)
            on update cascade on delete set null
);

INSERT INTO categories (id, title, description) VALUES
(3, 'Мебель', 'Мебель для работы :)'),
(4, 'Компьютерное оборудование', 'Компьютерное оборудование для работы :)'),
(5, 'Бытовая техника', 'Бытовая техника для кота :)'),
(7, 'Тест категория 2', 'Тест категория 2 описание');

INSERT INTO locations (id, title, description) VALUES
(1, 'Кабинет директора', 'Кабинет директора :)'),
(2, 'Офис 204', 'Офис 204 :)'),
(3, 'Учительская', 'Учительская :)');

INSERT INTO subjects (id, category_id, location_id, title, description, image) VALUES
(1, 3, 1, 'Стул', 'Стул в кабинете директора', 'np8pC-Dx1NSGbOJf6c_cN.png');
